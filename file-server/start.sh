#!/bin/sh

if [ $# -lt 1 ] ; then
	echo "Usage: ./start.sh <PORT>"
	exit 1
fi

TEST_PORT=$1

sudo docker run --rm --name test-nginx2 -v "$(pwd)/files:/usr/share/nginx/html" -d -p $TEST_PORT:80 nginx
